﻿using BlazorTp1.Models;

namespace BlazorTp1.Components
{
    public class CraftingRecipe
    {
        public Item Give { get; set; }
        public List<List<string>> Have { get; set; }
    }
}
