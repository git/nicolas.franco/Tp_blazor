﻿using BlazorTp1.Components;
using BlazorTp1.Models;
using System;

public interface IDataService
{
    Task Add(ItemModel model);

    Task<int> Count();

    Task<List<Item>> List(int currentPage, int pageSize);

    Task<Item> GetById(int id);

    Task Update(int id, ItemModel item);

    Task Delete(int id);

    Task<List<CraftingRecipe>> GetRecipes();
}
